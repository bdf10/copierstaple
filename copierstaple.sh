#!/usr/bin/env bash


LASTBOOKLET=0
INFILE="${@: -1}"

while getopts l:c:b: option
do
    case "${option}"
    in
	l) LASTBOOKLET=${OPTARG};;
	c) COPIERCODE=${OPTARG};;
	b) NBOOKLETS=${OPTARG};;
    esac
done

TOTALPAGES=$(pdfinfo $INFILE | grep Pages | awk '{print $2}')
NPAGES=$(($TOTALPAGES/$NBOOKLETS))
FIRSTPAGE=$(($NPAGES*$LASTBOOKLET+1))

echo LASTBOOKLET: $LASTBOOKLET
echo INFILE: $INFILE
echo COPIERCODE: $COPIERCODE
echo NBOOKLETS: $NBOOKLETS
echo TOTALPAGES: $TOTALPAGES
echo FIRSTPAGE: $FIRSTPAGE
echo NPAGES: $NPAGES

lpr -P lw2 \
    -o page-ranges=$FIRSTPAGE-$TOTALPAGES \
    -o Duplex=DuplexNoTumble \
    -o ARStaple=Staple1 \
    -o JCLCopierCode=Custom.$COPIERCODE \
    -o StapleN=Custom.$NPAGES $INFILE \
