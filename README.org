#+TITLE: copierstaple

A script for printing booklets to the copier. 

* Vanilla Command 

One may print stapled booklets from a large "master file" to the copier using
the following command.

Replace the following:

+ =$MYCOPIERCODE= with your copier code
+ =$NPAGES= with the number of pages per booklet
+ =$INFILE= with the large file to be printed

#+BEGIN_SRC sh
  lpr -P lw2 -o Duplex=DuplexNoTumple -o ARStaple=Staple1 -o JCLCopierCode=Custom.$MYCOPIERCODE -o StapleN=Custom.$NPAGES $INFILE
#+END_SRC

* =copierstaple= Script

The copier has trouble printing an entire assignment. Often, only =n= of the =N=
total booklets are printed. The script =copierstaple.sh= helps streamline the
process of getting started printing the =n+1=-st through =N=-th booklet.

The syntax for running =copierstaple= is:

#+BEGIN_SRC sh
  bash copierstaple.sh -c $MYCOPIERCODE -b $NBOOKLETS [-l $LASTBOOKLETNUMBER] $INFILE
#+END_SRC

Here, the flags are:

+ *-c* The user's copiercode.
+ *-b* The total number of booklets in the "master file."
+ *-l* The last booklet printed by the copier. This flag is optional and
  defaults to =0=.

** Example

Suppose the file =~/path/to/teaching/202s18-exam01--100-booklets.pdf= contains
each of the =100= booklets for an exam. Also suppose that the user's copier code
is =8675309=.

To start printing each of the stapled booklets, the user would issue the command:

#+BEGIN_SRC sh
  bash copierstaple.sh -c 8675309 -b 100 ~/path/to/teaching/202s18-exam01--100-booklets.pdf
#+END_SRC

The copier will start printing the stapled booklets. 

Suppose that the copier only gets through booklet number =47=. The user should
then issue the command

#+BEGIN_SRC sh
  bash copierstaple.sh -c 8675309 -b 100 -l 47 ~/path/to/teaching/202s18-exam01--100-booklets.pdf
#+END_SRC

The copier will then start printing the =48=-th booklet.
